﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Q3Network
{
	public enum WeaponType:byte {
		None,
		Gauntlet,
		Machinegun,
		Shotgun,
		GrenadeLauncher,
		RocketLauncher,
		Lightning,
		Railgun,
		Plasmagun,
		Bfg,
		GrapplingHook,
		// <MISSIONPACK>
		Nailgun,
		ProxLauncher,
		Chaingun,
		// </MISSIONPACK>
	}
}
